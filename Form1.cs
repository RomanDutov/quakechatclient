﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading;

namespace ClientChat
{
    public partial class Form1 : Form
    {
        public Socket Client;
        private IPAddress ip = null;
        private int port = 0;
        private Thread thread;


        public Form1()
        {
            InitializeComponent();

            chatRichBox.Enabled = false;
            sendMessageButton.Enabled = false;
            messageRichBox.Enabled = false;

            try
            {
                StreamReader stream = new StreamReader(@"ClientInfo/DataInfo.txt");
                string buffer = stream.ReadToEnd();
                stream.Close();

                string[] connectInfo = buffer.Split(':');

                ip = IPAddress.Parse(connectInfo[0]);
                port = int.Parse(connectInfo[1]);

                label7.ForeColor = Color.Green;
                label7.Text = "Configuraiotn: \n IP server" + connectInfo[0] + " \n Port: " + connectInfo[1];


            }
            catch (Exception ex)
            {

                label7.ForeColor = Color.Red;
                label7.Text = "Configuraiotn not found";

                ConfigForm form = new ConfigForm();
                form.Show();

            }
        }

        private void menuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void configToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfigForm form = new ConfigForm();
            form.Show();

        }


        void SendMessage(string message)
        {
            if (message != "")
            {
                byte[] buffer = new byte[1024];
                buffer = Encoding.UTF8.GetBytes(message);
                Client.Send(buffer);
            }
        }

        void RecivedMessage()
        {
            byte[] buffer = new byte[1024];
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = 0;
            }
            while (true)
            {
                try
                {
                    Client.Receive(buffer);
                    string message = Encoding.UTF8.GetString(buffer);
                    int count = message.IndexOf(";;;5");
                    if (count == -1)
                    {
                        continue;
                    }
                    string clearMessage = "";
                    for (int i = 0; i < count; i++)
                    {
                        clearMessage += message[i];
                    }
                    for (int i = 0; i < buffer.Length; i++)
                    {
                        buffer[i] = 0;
                    }

                    this.Invoke((MethodInvoker) delegate()
                    {
                        messageRichBox.AppendText(clearMessage);
                    });
                }
                catch (Exception ex)
                {
                    
                    
                }

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            chatRichBox.Enabled = true;
            sendMessageButton.Enabled = true;
            messageRichBox.Enabled = true;
            Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            if (ip != null){

                Client.Connect(ip, port);
                thread = new Thread(delegate()
                {
                    RecivedMessage();
                });
                thread.Start();

                messageRichBox.Focus();
            }
                                   
        }

        private void sendMessageButton_Click(object sender, EventArgs e)
        {
            SendMessage("\n"+ nickTextBox.Text + ": " + messageRichBox.Text + ";;;5");
            messageRichBox.Clear();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (thread != null)
            {
                thread.Abort();
            }
            if (Client != null)
            {
                Client.Close();
            }
            Application.Exit();
        }

        private void messageRichBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendMessage("\n" + nickTextBox.Text + ": " + messageRichBox.Text + ";;;5");
                messageRichBox.Clear();
            }
        }
    }
}
