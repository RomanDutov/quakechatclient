﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientChat
{
    public partial class ConfigForm : Form
    {
        public ConfigForm()
        {
            InitializeComponent();
        }

        private void saveConfigButton_Click(object sender, EventArgs e)
        {
            if (ipTextBox.Text != "" && portTextBox.Text != "")
            {
                try
                {
                    DirectoryInfo dir = new DirectoryInfo("ClientInfo");
                    dir.Create();

                    var streamWrite = new StreamWriter(@"ClientInfo/DataInfo.txt");
                    streamWrite.WriteLine(ipTextBox.Text + ":" + portTextBox.Text);
                    streamWrite.Close();

                    this.Hide();

                    Application.Restart();
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Error" + ex.ToString(), "Error");

                }
               
            }
        }
    }
}
